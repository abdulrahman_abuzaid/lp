clear all
clc
close all

Food = {'Rice';
'Quinoa';
'Tortilla';
'Lentils';
'Broccoli'};

Nutrients = {
'Carbohydrates';
'Protein';
'Fat'};

AmountofNutrients_PerFood = [53 4.4 0.4
    40 8 3.6
    12 3 2
    53 12 0.9
    6 1.9 0.3];

Cost = [0.5 0.9 0.1 0.6 0.4]';

Min_Nutrients = [100 10 0]';

Max_Nutrients = [1000 100 100];

% cvx_begin
% variables x(length(Cost));
% minimize transpose(Cost)*x
% subject to
% Min_Nutrients<=transpose(AmountofNutrients_PerFood)*x<=Max_Nutrients;
% x>=0;
% cvx_end
% I1 =find(x>1e-6);
% %x(I1)
% for ii=1:length(I1)
%     disp(Food{I1(ii)})
% end


Sol = linprog(Cost,[AmountofNutrients_PerFood';-AmountofNutrients_PerFood'],[Max_Nutrients;-Min_Nutrients],[],[],zeros(1,64));
I2 = find(Sol>1e-6);
disp(Cost'*Sol);
Final = Sol(I2);
for ii=1:length(I2)
    disp(Food{I2(ii)})
    disp(Final(ii))
end

