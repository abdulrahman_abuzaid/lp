function writeData(filename, enter, leave, z0)
fid = fopen(filename, 'w');
if ~(isempty(leave))
    fprintf(fid, '%i', enter);
    fprintf(fid, '\n');
    fprintf(fid, '%i', leave);
    fprintf(fid, '\n');
    fprintf(fid, '%f', z0);
else
    fprintf(fid, '%s', 'UNBOUNDED');
end
end

