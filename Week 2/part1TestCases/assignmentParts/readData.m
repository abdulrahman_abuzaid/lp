function [B, N, b, A, z0, c] = readData(filename)
fid = fopen(filename);
m = fscanf(fid, '%u', 1);
n = fscanf(fid, '%u', 1);
B = fscanf(fid, '%u', m);
N = fscanf(fid, '%u', n)';
b = fscanf(fid, '%f', m);
A = fscanf(fid, '%f', n*m)';
A = reshape(A, n, m)';
z0 = fscanf(fid, '%f', 1);
c = fscanf(fid, '%f', n)';
end