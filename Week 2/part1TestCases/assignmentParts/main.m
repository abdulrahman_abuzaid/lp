for fileNumber = 1:5
filename = sprintf('part%i.dict', fileNumber);
[B, N, b, A, z0, c] = readData(filename);
enter = min(N(c > 0));
enterLoc = find(N == enter);
leaveLoc = find(A(:, enterLoc) < 0);
incrAmountAll = -b(leaveLoc) ./ A(leaveLoc, enterLoc);
incrAmountMin = min(incrAmountAll);
incrAmountMinLoc = find(incrAmountAll == incrAmountMin);
BTemp = B(leaveLoc);
leave = min(BTemp(incrAmountMinLoc));

if ~isempty(leave)
    finalLeaveLoc = find(B == leave);
    z0 = z0-b(finalLeaveLoc) * c(enterLoc)...
        / A(finalLeaveLoc, enterLoc);
end
filename = sprintf('part%i.dict.out', fileNumber);
writeData(filename, enter, leave, z0);
end


