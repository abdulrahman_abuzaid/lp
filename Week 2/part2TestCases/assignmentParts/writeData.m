function writeData(filename, unbounded, iter, z0)
fid = fopen(filename, 'w');
if (~unbounded)
    fprintf(fid, '%f', z0);
    fprintf(fid, '\n');
    fprintf(fid, '%i', iter);
    fprintf(fid, '\n');
    
else
    fprintf(fid, '%s', 'UNBOUNDED');
end
end

