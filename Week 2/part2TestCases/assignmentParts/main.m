tic
for fileNumber = 1:5  
filename = sprintf('part%i.dict', fileNumber);
[B, N, b, A, z0, c] = readData(filename);
iter = 0;
unbounded = false;
while (true)
    enter = min(N(c > 0));
    if (isempty(enter))
        break;
    end
    j = find(N == enter);
    leaveLoc = find(A(:, j) < 0);
    incrAmountAll = -b(leaveLoc) ./ A(leaveLoc, j);
    incrAmountMin = min(incrAmountAll);
    incrAmountMinLoc = find(incrAmountAll == incrAmountMin);
    BTemp = B(leaveLoc);
    leave = min(BTemp(incrAmountMinLoc));

    if ~isempty(leave)
        iter = iter+1;
        i = find(B == leave);
        z0 = z0 - (b(i) * c(j)...
            / A(i, j));
        tempcj = c(j);
        c = c - A(i, :) * tempcj / A(i, j);
        c(j) = tempcj / A(i, j);
        tempbi = b(i);
        b = b - tempbi * A(:, j) / A(i, j);
        b(i) = - tempbi / A(i, j);
        tempA = A;
        for k = 1:size(A,1)
            A(k, :) = tempA(k, :) - tempA(i, :)...
                * tempA(k, j) / tempA(i, j);
        end
        A(:, j) = tempA(:, j) / tempA(i, j);
        A(i, :) = -tempA(i, :) / tempA(i, j);
        A(i, j) = 1 / tempA(i, j);
        B(i) = enter;
        N(j) = leave;
    else
        unbounded = true;
        break;
    end
    [B b A; 0 z0 c; 0 0 N];
end
[B b A; 0 z0 c; 0 0 N];
unbounded;
iter;
z0;
filename = sprintf('part%i.out', fileNumber);
writeData(filename, unbounded, iter, z0);

end
toc

